$(function(){

	// Get the current year for the copyright
	$('#year').text(new Date().getFullYear());

	CKEDITOR.replace('editor1');

	//Modals
	$('#modal').on('click', function(){
		$('#modal').modal('hide');
	});

	// Highlight las secciones del navbar
	var header = document.getElementById("navbarSupportedContent");
	var nav_btns = header.getElementsByClassName("nav-item");
	for (var i = 0; i < nav_btns.length; i++) {
	  nav_btns[i].addEventListener("click", function() {
	    var current = document.getElementsByClassName("active");
	    current[0].className = current[0].className.replace(" active", "");
	    this.className += " active";
	  });
	}

	//modals effect
	$('input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $("input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });


})
